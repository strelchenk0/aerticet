<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('carrier_id');
            $table->foreign('carrier_id')->references('id')->on('carriers');

            $table->unsignedBigInteger('departure_id');
            $table->foreign('departure_id')->references('id')->on('airports');

            $table->unsignedBigInteger('arrival_id');
            $table->foreign('arrival_id')->references('id')->on('airports');

            $table->string('number');
            $table->dateTime('departure_data_time');
            $table->dateTime('arrival_date_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->dropForeign(['carrier_id', 'departure_id', 'arrival_id']);
        });

        Schema::dropIfExists('flights');
    }
}
