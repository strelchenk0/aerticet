@extends('front.layout')

@section('scripts')
    <script>window.page="home"</script>
@endsection

@section('main')

    <form id="search_flight" role="form" method="POST" action="#">
        <div class="row">
            <div class="col-12"><h1>Air ticket search</h1></div>
            <div class="col-3">
                <label for="departureAirport">Departure airport:</label>
                <select name="departureAirport" class="form-control" required>
                    @foreach($airports as $airport)
                        <option value="{{ $airport->id }}">{{ $airport->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-3">
                <label for="arrivalAirport">Arrival airport:</label>
                <select name="arrivalAirport" class="form-control" required>
                    @foreach($airports as $airport)
                        <option value="{{ $airport->id }}">{{ $airport->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-3">
                <label for="departureDate">Departure date:</label>
                <input type="date" name="departureDate" class="form-control" required>
            </div>
            <div class="col-3 btn-container">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </div>
        <div class="container ">
            <table class="table display dn">
                <thead>
                <tr>
                    <th scope="col">Carrier</th>
                    <th scope="col">Flight num</th>
                    <th scope="col">Departure date time</th>
                    <th scope="col">Arrival date time</th>
                    <th scope="col">flight time (min)</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>


    </form>
@endsection