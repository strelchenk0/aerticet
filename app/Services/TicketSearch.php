<?

namespace App\Services;

use App\Repositories\FlightRepository;
use App\Models\Flight;

class TicketSearch {

    protected $repository;

    /**
     * Ticket constructor.
     */
    public function __construct()
    {
        $this->repository = new FlightRepository(new Flight());
    }

    /**
     * @param $data
     * @return array
     */
    public function searchRequest($data){

        $response = $this->repository->search($data);

        return [
            'message' => $response->toArray(),
            'errors' => ''
        ];
    }








}


