<?php

namespace App\Repositories;

use App\Models\Flight;


class FlightRepository
{

    /**
     * The Model instance.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;


    /**
     * AirportRepository constructor.
     * @param Flight $flight
     */
    public function __construct(Flight $flight)
    {
        $this->model = $flight;
    }

    /**
     * @param $parameters
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * @param $data
     * @return mixed
     */
    public function search($data){

        $collect = $this->model::where('departure_id', $data['departureAirport'])
            ->where('arrival_id', $data['arrivalAirport'])
            ->whereDate('departure_data_time', '=', $data['departureDate'])
            ->orderBy('departure_data_time', 'asc')
            ->get();

        return $collect;
    }

}
