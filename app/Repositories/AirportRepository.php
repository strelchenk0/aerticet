<?php

namespace App\Repositories;

use App\Models\Airport;


class AirportRepository
{

    /**
     * The Model instance.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;


    /**
     * AirportRepository constructor.
     * @param Airport $airport
     */
    public function __construct(Airport $airport)
    {
        $this->model = $airport;
    }

    /**
     * @param $parameters
     * @return mixed
     */
    public function getAll()
    {
        return $this->model->all();
    }
}
