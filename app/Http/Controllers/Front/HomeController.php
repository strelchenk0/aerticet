<?php

namespace App\Http\Controllers\Front;

use App\Repositories\AirportRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{


    /**
     * Display a listing of the posts.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, AirportRepository $airportRepository)
    {
        $airports = $airportRepository->getAll();

        return view('front.search', compact('airports'));
    }




}
