<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\TicketSearch;
use Illuminate\Http\Request;
use Exception;


class TicketController extends Controller
{

    /**
     * @param Request $request
     * @param TicketSearch $ticketSearch
     * @return \Illuminate\Http\JsonResponse
     * @throws Exception
     */
    public function search(Request $request, TicketSearch $ticketSearch)
    {
        $request->validate([
            'departureAirport' => 'required|numeric|exists:airports,id',
            'arrivalAirport' => 'required|numeric|exists:airports,id|not-in:' . $request->departureAirport,
            'departureDate' => 'required|date|after:yesterday',
        ]);

        $response = $ticketSearch->searchRequest($request->all());

        if(!count($response['message']))
            throw new Exception('Flights not found');

        return response()->json($response);

    }


}
