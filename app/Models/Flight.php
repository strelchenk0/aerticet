<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Flight extends Model
{

    protected $table = 'flights';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'carrier_id', 'departure_id', 'arrival_id', 'number', 'departure_data_time', 'arrival_date_time'
    ];

    /**
     * @var array
     */
    protected $appends = ['carrier_model', 'departure_model', 'arrival_model', 'flight_time'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getCarrierModelAttribute()
    {
        return $this->carrier;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getDepartureModelAttribute()
    {
        return $this->departure;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getArrivalModelAttribute()
    {
        return $this->arrival;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Carrier(){
        return $this->hasOne(Carrier::class,  'id', 'carrier_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Departure(){
        return $this->hasOne(Airport::class,  'id', 'departure_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function Arrival(){
        return $this->hasOne(Airport::class,  'id', 'arrival_id');
    }


    /**
     * @return float|int
     */
    public function getFlightTimeAttribute()
    {
        return $this->get_time_difference_in_local_time_zones() + $this->get_time_zones_diff();
    }

    /**
     * @return float|int
     */
    protected function get_time_difference_in_local_time_zones()
    {
        $flight_time = \DB::table($this->table)
            ->selectRaw('TIMEDIFF(`arrival_date_time`, `departure_data_time`) as time ')
            ->where('id', $this->id)
            ->first();

        $time = explode(':', $flight_time->time);
        return ($time[0]*60) + ($time[1]) + ($time[2]/60);
    }

    /**
     * @return float
     */
    protected function get_time_zones_diff() : float
    {
        return (float) ($this->departure_model->timezone - $this->arrival_model->timezone) * 60;
    }

}
