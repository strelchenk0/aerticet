<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Airport extends Model
{

    protected $table = 'airports';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'timezone'
    ];


}
