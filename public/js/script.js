var home = function () {
    var self = this;

    var serialize = function () {
        return {
            departureAirport: self.departureAirport.val(),
            arrivalAirport: self.arrivalAirport.val(),
            departureDate: self.departureDate.val(),
        };
    };

    var respanceSuccess = function (res) {
        drowTickets(res.message);
    };

    var drowTickets = function (tickets) {
        var html = '';
        for (var prop in tickets) {
            var ticket = tickets[prop];
            html += '<tr>' +
                '<th scope="row">'+ ticket.carrier_model.name +'</th>' +
                '<td>'+ ticket.number +'</td>' +
                '<td>'+ ticket.departure_data_time +'</td>' +
                '<td>'+ ticket.arrival_date_time +'</td>' +
                '<td>'+ ticket.flight_time +'</td>' +
                '</tr>';
        }

        self.dispaly_body.html(html);
        self.dispaly.removeClass('dn');
    };

    var search = function(e){
        e.preventDefault();

        var data = serialize();

        $.ajax({
            type: "POST",
            url: '/api/v1/search',
            data: data,
            success: respanceSuccess,
            error: function (error) {
                self.dispaly.addClass('dn');
                alert(error.responseJSON.message + ' Detail in console');
                console.log(error.responseJSON);
            }
        });

    };

    // Init
    self.search_form = $('#search_flight');
    self.departureAirport = self.search_form.find("*[name=departureAirport]");
    self.arrivalAirport = self.search_form.find("*[name=arrivalAirport]");
    self.departureDate = self.search_form.find("*[name=departureDate]");
    self.dispaly = $('.display');
    self.dispaly_body = self.dispaly.find('tbody');

    self.search_form.submit(search);

};

$( document ).ready(function() {
    window.controller = new window[window.page];
});